import React, { FunctionComponent, useEffect, useState } from 'react';
import { AppBar, Container, Tab, Tabs, Grid, Fab } from '@material-ui/core';
import { v4 as uuid } from 'uuid';
import ProductTypeEnum from '@warehouse/domain/entities/enums/productType.enum';
import { getStockItemsRequest, getStockItemsSuccess } from '@warehouse/presentation/adapters/redux/stock/stock.actions';
import { useWebsocket } from '@core/presentation/contexts/websocket.context';
import { selectStockItems } from '@warehouse/presentation/adapters/redux/stock/stock.selectors';
import { useDispatch, useSelector } from 'react-redux';
import RefreshIcon from '@material-ui/icons/Refresh';
import Loader from '@core/presentation/components/loader/Loader';
import Toast from '@core/presentation/components/alert/Toast';
import { selectScheduleState } from '@warehouse/presentation/adapters/redux/schedule/schedule.selectors';
import {
  getScheduleRequest,
  getScheduleSuccess,
} from '@warehouse/presentation/adapters/redux/schedule/schedule.actions';
import WebsocketResponse from '@warehouse/domain/entities/websocketResponse.model';
import useStyles, { StyledTypography } from './Warehouse.style';
import TabPanel from './components/tabPanel/TabPanel';
import TabTable from './components/tabTable/TabTable';

const Warehouse: FunctionComponent = () => {
  const classes = useStyles();
  const DEFAULT_PRODUCT_TYPES = Object.values(ProductTypeEnum);
  const [activeTab, setActiveTab] = useState(0);
  const dispatch = useDispatch();
  const stockItems = useSelector(selectStockItems);
  const scheduleState = useSelector(selectScheduleState);
  const socket = useWebsocket();

  useEffect(() => {
    dispatch(getScheduleRequest());
    dispatch(getStockItemsRequest(Object.values(ProductTypeEnum)));
    socket?.on('stock', (response: WebsocketResponse) => {
      const { isActive, lastUpdate, nextUpdate } = response.data.schedule;
      dispatch(
        getScheduleSuccess({
          isActive,
          lastUpdate: lastUpdate ? new Date(lastUpdate) : null,
          nextUpdate: new Date(nextUpdate),
        })
      );
      dispatch(getStockItemsSuccess(response.data.stock));
    });
    return () => {
      socket?.off('stock');
    };
  }, [socket, dispatch]);

  const a11yProps = (index: number) => ({
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  });

  return (
    <Container maxWidth={false} className={classes.container}>
      <Loader />
      <Toast />
      <AppBar position='static' color='default'>
        <Tabs
          value={activeTab}
          onChange={(ev, value) => setActiveTab(value)}
          indicatorColor='primary'
          textColor='primary'
          variant='scrollable'
          scrollButtons='auto'
          aria-label='scrollable auto tabs example'
        >
          {DEFAULT_PRODUCT_TYPES.map((productType, index) => (
            <Tab key={uuid()} label={productType} {...a11yProps(index)} />
          ))}
        </Tabs>
      </AppBar>
      <Grid container justify='center' alignItems='center'>
        <Grid item xs={12}>
          <StyledTypography variant='body2'>
            Last update:{' '}
            {scheduleState.lastUpdate ? scheduleState.lastUpdate.toLocaleTimeString() : 'Pending initial sync'}
          </StyledTypography>
          <StyledTypography variant='body2'>
            Next automatic update:{' '}
            {scheduleState.nextUpdate ? scheduleState.nextUpdate.toLocaleTimeString() : 'Pending initial sync'}
          </StyledTypography>
        </Grid>
        <Grid item xs={12}>
          {DEFAULT_PRODUCT_TYPES.map((productType, index) => {
            return (
              <TabPanel key={uuid()} value={activeTab} index={index}>
                <TabTable rows={stockItems.filter((stockItem) => stockItem.type === productType)} />
              </TabPanel>
            );
          })}
        </Grid>
        <Grid item xs={12}>
          <Fab
            color='primary'
            aria-label='refresh'
            className={classes.fab}
            onClick={() => dispatch(getStockItemsRequest(DEFAULT_PRODUCT_TYPES))}
          >
            <RefreshIcon />
          </Fab>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Warehouse;
