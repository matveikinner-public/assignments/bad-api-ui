import { Grid, styled, withTheme, Typography } from '@material-ui/core';
import { createStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      padding: '0',
      height: '100vh',
    },
    tableContainer: {
      height: '500px',
    },
    fab: {
      position: 'absolute',
      bottom: 0,
      right: 0,
      margin: theme.spacing(2),
    },
  })
);

export const StyledTypography = styled(withTheme(Typography))(({ theme }) => ({
  margin: theme.spacing(2),
}));

export default useStyles;
