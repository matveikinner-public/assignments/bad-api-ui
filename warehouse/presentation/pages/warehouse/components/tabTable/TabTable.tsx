import React, { useState } from 'react';
import {
  TableContainer,
  Paper,
  TableRow,
  TableCell,
  TableHead,
  TableBody,
  Table,
  TablePagination,
} from '@material-ui/core';
import { v4 as uuid } from 'uuid';
import { Skeleton } from '@material-ui/lab';
import TabTableProps from './TabTable.types';
import useStyles from './TabTable.style';

const TabTable = <T extends Record<keyof T, T[keyof T]>>({ rows }: TabTableProps<T>): JSX.Element => {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const CELL_MIN_WIDTH = 128;
  const CELL_MAGIC_SPACING = 10;

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  // To properly typecheck if generic can have toString() method is impossible in this scenario?
  const getColumnWidth = (tempRows: Array<T>, accessor: Extract<keyof T, string>) =>
    // eslint-disable-next-line
    Math.max(
      CELL_MIN_WIDTH,
      Math.max(...tempRows.map((row) => (row[accessor] ? row[accessor].toString().length * CELL_MAGIC_SPACING : '')))
    );

  return (
    <>
      <TableContainer component={Paper} className={classes.tableContainer}>
        <Table stickyHeader aria-label='caption table' style={{ tableLayout: 'fixed' }}>
          <TableHead>
            <TableRow>
              {rows.length ? (
                (Object.keys(rows[0]) as Array<Extract<keyof T, string>>).map((column) => (
                  <TableCell
                    key={uuid()}
                    className={classes.tableCell}
                    style={{ width: `${getColumnWidth(rows, column)}px` }}
                  >
                    {column.toUpperCase()}
                  </TableCell>
                ))
              ) : (
                <TableCell className={classes.tableCell}>
                  <Skeleton variant='rect' />
                </TableCell>
              )}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.length
              ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                  return (
                    <TableRow key={uuid()} hover role='checkbox' tabIndex={-1}>
                      {Object.values<keyof T>(row).map((value) => {
                        return (
                          <TableCell key={uuid()} className={classes.tableCell}>
                            {Array.isArray(value) ? value.join(', ') : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })
              : Array.from(new Array<T>(rowsPerPage)).map(() => (
                  <TableRow key={uuid()} hover role='checkbox' tabIndex={-1}>
                    <TableCell align='right'>
                      <Skeleton variant='rect' />
                    </TableCell>
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component='div'
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </>
  );
};

export default TabTable;
