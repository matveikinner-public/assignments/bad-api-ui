interface TabTableProps<T> {
  rows: Array<T>;
}

export default TabTableProps;
