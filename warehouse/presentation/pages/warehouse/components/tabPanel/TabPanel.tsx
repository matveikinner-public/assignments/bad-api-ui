import React, { FunctionComponent } from 'react';
import useStyles from './TabPanel.style';
import TabPanelProps from './TabPanel.types';

const TabPanel: FunctionComponent<TabPanelProps> = ({ children, value, index }: TabPanelProps) => {
  const classes = useStyles();
  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`wrapped-tabpanel-${index}`}
      aria-labelledby={`wrapped-tab-${index}`}
      className={classes.root}
    >
      {value === index && <>{children}</>}
    </div>
  );
};

export default TabPanel;
