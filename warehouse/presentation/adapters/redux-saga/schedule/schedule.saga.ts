import { createLoader, removeLoader } from '@core/presentation/adapters/redux/loader/loader.actions';
import { LoaderActionTypes } from '@core/presentation/adapters/redux/loader/loader.types';
import { createErrorToast, createSuccessToast } from '@core/presentation/adapters/redux/toast/toast.actions';
import { ToastActionTypes } from '@core/presentation/adapters/redux/toast/toast.types';
import Schedule from '@warehouse/domain/entities/schedule.model';
import IWarehouseRepository from '@warehouse/domain/interfaces/warehouse.repository.interface';
import { getContext, GetContextEffect, put, PutEffect } from 'redux-saga/effects';
import { getScheduleFailure, getScheduleSuccess } from '../../redux/schedule/schedule.actions';
import { ScheduleActionTypes } from '../../redux/schedule/schedule.types';

export default function* getScheduleRequestSaga(): Generator<
  | PutEffect<LoaderActionTypes>
  | PutEffect<ToastActionTypes>
  | PutEffect<ScheduleActionTypes>
  | GetContextEffect
  | Promise<Schedule>,
  void,
  IWarehouseRepository & Schedule
> {
  yield put(createLoader());
  const warehouseInteractor: IWarehouseRepository = (yield getContext('warehouseInteractor')) as IWarehouseRepository;

  try {
    const { isActive, lastUpdate, nextUpdate } = (yield warehouseInteractor.getSchedule()) as Schedule;
    yield put(
      getScheduleSuccess({
        isActive,
        lastUpdate: lastUpdate ? new Date(lastUpdate) : null,
        nextUpdate: new Date(nextUpdate),
      })
    );
    yield put(createSuccessToast('Success while attempting to retrieve warehouse schedule'));
  } catch (err) {
    yield put(getScheduleFailure());
    yield put(createErrorToast('Error while attempting to retrieve warehouse schedule'));
  } finally {
    yield put(removeLoader());
  }
}
