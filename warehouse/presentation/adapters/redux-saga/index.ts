import { all, AllEffect, ForkEffect, takeLatest } from 'redux-saga/effects';
import { GET_SCHEDULE_REQUEST } from '../redux/schedule/schedule.constants';
import { GET_STOCK_ITEMS_REQUEST, SET_STOCK_ITEMS_REQUEST } from '../redux/stock/stock.constants';
import getScheduleRequestSaga from './schedule/schedule.saga';
import { getStockItemsRequestSaga, setStockItemsRequestSaga } from './stock/stock.saga';

export default function* root(): Generator<AllEffect<ForkEffect<never>>, void, unknown> {
  yield all([
    takeLatest(GET_SCHEDULE_REQUEST, getScheduleRequestSaga),
    takeLatest(GET_STOCK_ITEMS_REQUEST, getStockItemsRequestSaga),
    takeLatest(SET_STOCK_ITEMS_REQUEST, setStockItemsRequestSaga),
  ]);
}
