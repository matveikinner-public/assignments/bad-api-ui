import { createLoader, removeLoader } from '@core/presentation/adapters/redux/loader/loader.actions';
import { LoaderActionTypes } from '@core/presentation/adapters/redux/loader/loader.types';
import { createErrorToast, createSuccessToast } from '@core/presentation/adapters/redux/toast/toast.actions';
import { ToastActionTypes } from '@core/presentation/adapters/redux/toast/toast.types';
import StockItem from '@warehouse/domain/entities/stockItem.model';
import IWarehouseRepository from '@warehouse/domain/interfaces/warehouse.repository.interface';
import { getContext, GetContextEffect, put, PutEffect } from 'redux-saga/effects';
import { setScheduleLastUpdate } from '../../redux/schedule/schedule.actions';
import { ScheduleActionTypes } from '../../redux/schedule/schedule.types';
import { getStockItemsFailure, getStockItemsSuccess } from '../../redux/stock/stock.actions';
import { GetStockItemsRequest, SetStockItemsRequest, StockActionTypes } from '../../redux/stock/stock.types';

export function* getStockItemsRequestSaga({
  payload,
}: GetStockItemsRequest): Generator<
  | PutEffect<ToastActionTypes>
  | PutEffect<LoaderActionTypes>
  | PutEffect<ScheduleActionTypes>
  | GetContextEffect
  | Promise<StockItem[]>
  | PutEffect<StockActionTypes>,
  void,
  IWarehouseRepository & StockItem[]
> {
  yield put(createLoader());
  const warehouseInteractor: IWarehouseRepository = (yield getContext('warehouseInteractor')) as IWarehouseRepository;

  try {
    const stock = (yield warehouseInteractor.getStockItems(payload)) as StockItem[];
    yield put(getStockItemsSuccess(stock));
    yield put(setScheduleLastUpdate(new Date()));
    yield put(createSuccessToast('Success while attempting to retrieve stock'));
  } catch (err) {
    yield put(getStockItemsFailure());
    yield put(createErrorToast('Error while attempting to retrieve stock'));
  } finally {
    yield put(removeLoader());
  }
}

export function* setStockItemsRequestSaga({
  payload,
}: SetStockItemsRequest): Generator<
  PutEffect<StockActionTypes> | PutEffect<ToastActionTypes> | PutEffect<LoaderActionTypes>,
  void,
  unknown
> {
  yield put(getStockItemsSuccess(payload));
  yield put(createSuccessToast('Success while attempting to automatically update stock'));
  yield put(removeLoader());
}
