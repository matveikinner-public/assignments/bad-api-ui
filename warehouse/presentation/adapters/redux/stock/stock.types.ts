import ProductTypeEnum from '@warehouse/domain/entities/enums/productType.enum';
import StockItem from '@warehouse/domain/entities/stockItem.model';
import {
  GET_STOCK_ITEMS_REQUEST,
  GET_STOCK_ITEMS_SUCCESS,
  GET_STOCK_ITEMS_FAILURE,
  SET_STOCK_ITEMS_REQUEST,
} from './stock.constants';

export interface GetStockItemsRequest {
  type: typeof GET_STOCK_ITEMS_REQUEST;
  payload: ProductTypeEnum[];
}

interface GetStockItemsSuccess {
  type: typeof GET_STOCK_ITEMS_SUCCESS;
  payload: StockItem[];
}

interface GetStockItemsFailure {
  type: typeof GET_STOCK_ITEMS_FAILURE;
}

export interface SetStockItemsRequest {
  type: typeof SET_STOCK_ITEMS_REQUEST;
  payload: StockItem[];
}

export type StockActionTypes =
  | GetStockItemsRequest
  | GetStockItemsSuccess
  | GetStockItemsFailure
  | SetStockItemsRequest;
