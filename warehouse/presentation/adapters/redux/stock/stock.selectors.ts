import { RootState } from '@core/presentation/frameworks/redux.config';
import StockItem from '@warehouse/domain/entities/stockItem.model';

// eslint-disable-next-line import/prefer-default-export
export const selectStockItems = (state: RootState): StockItem[] => state.warehouse.stock;
