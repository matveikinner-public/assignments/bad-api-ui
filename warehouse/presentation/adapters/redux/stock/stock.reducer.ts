import StockItem from '@warehouse/domain/entities/stockItem.model';
import {
  GET_STOCK_ITEMS_REQUEST,
  GET_STOCK_ITEMS_SUCCESS,
  GET_STOCK_ITEMS_FAILURE,
  SET_STOCK_ITEMS_REQUEST,
} from './stock.constants';
import { StockActionTypes } from './stock.types';

const initialState: StockItem[] = [];

const stockReducer = (state = initialState, action: StockActionTypes): StockItem[] => {
  switch (action.type) {
    case GET_STOCK_ITEMS_REQUEST:
      return state;
    case GET_STOCK_ITEMS_SUCCESS:
      return action.payload;
    case GET_STOCK_ITEMS_FAILURE:
      return state;
    case SET_STOCK_ITEMS_REQUEST:
      return action.payload;
    default:
      return state;
  }
};

export default stockReducer;
