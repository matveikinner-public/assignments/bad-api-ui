import ProductTypeEnum from '@warehouse/domain/entities/enums/productType.enum';
import StockItem from '@warehouse/domain/entities/stockItem.model';
import {
  GET_STOCK_ITEMS_REQUEST,
  GET_STOCK_ITEMS_SUCCESS,
  GET_STOCK_ITEMS_FAILURE,
  SET_STOCK_ITEMS_REQUEST,
} from './stock.constants';
import { StockActionTypes } from './stock.types';

export const getStockItemsRequest = (payload: ProductTypeEnum[]): StockActionTypes => ({
  type: GET_STOCK_ITEMS_REQUEST,
  payload,
});

export const getStockItemsSuccess = (payload: StockItem[]): StockActionTypes => ({
  type: GET_STOCK_ITEMS_SUCCESS,
  payload,
});

export const getStockItemsFailure = (): StockActionTypes => ({
  type: GET_STOCK_ITEMS_FAILURE,
});

export const setStockItemsRequest = (payload: StockItem[]): StockActionTypes => ({
  type: SET_STOCK_ITEMS_REQUEST,
  payload,
});
