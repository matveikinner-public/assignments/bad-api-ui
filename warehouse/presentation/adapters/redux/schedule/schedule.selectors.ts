import { RootState } from '@core/presentation/frameworks/redux.config';
import { ScheduleState } from './schedule.types';

// eslint-disable-next-line import/prefer-default-export
export const selectScheduleState = (state: RootState): ScheduleState => state.warehouse.schedule;
