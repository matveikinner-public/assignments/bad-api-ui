import {
  GET_SCHEDULE_FAILURE,
  GET_SCHEDULE_REQUEST,
  GET_SCHEDULE_SUCCESS,
  SET_SCHEDULE_LAST_UPDATE,
} from './schedule.constants';
import { ScheduleActionTypes, ScheduleState } from './schedule.types';

const initialState: ScheduleState = <ScheduleState>{};

const schedulerReducer = (state = initialState, action: ScheduleActionTypes): ScheduleState => {
  switch (action.type) {
    case GET_SCHEDULE_REQUEST:
      return state;
    case GET_SCHEDULE_SUCCESS:
      return action.payload;
    case GET_SCHEDULE_FAILURE:
      return state;
    case SET_SCHEDULE_LAST_UPDATE:
      return {
        ...state,
        lastUpdate: action.payload,
      };
    default:
      return state;
  }
};

export default schedulerReducer;
