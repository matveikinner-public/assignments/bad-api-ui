import {
  GET_SCHEDULE_FAILURE,
  GET_SCHEDULE_REQUEST,
  GET_SCHEDULE_SUCCESS,
  SET_SCHEDULE_LAST_UPDATE,
} from './schedule.constants';
import { ScheduleActionTypes, ScheduleState } from './schedule.types';

export const getScheduleRequest = (): ScheduleActionTypes => ({
  type: GET_SCHEDULE_REQUEST,
});

export const getScheduleSuccess = (payload: ScheduleState): ScheduleActionTypes => ({
  type: GET_SCHEDULE_SUCCESS,
  payload,
});

export const getScheduleFailure = (): ScheduleActionTypes => ({
  type: GET_SCHEDULE_FAILURE,
});

export const setScheduleLastUpdate = (payload: Date): ScheduleActionTypes => ({
  type: SET_SCHEDULE_LAST_UPDATE,
  payload,
});
