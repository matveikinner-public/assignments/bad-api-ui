import {
  GET_SCHEDULE_FAILURE,
  GET_SCHEDULE_REQUEST,
  GET_SCHEDULE_SUCCESS,
  SET_SCHEDULE_LAST_UPDATE,
} from './schedule.constants';

export interface GetScheduleRequest {
  type: typeof GET_SCHEDULE_REQUEST;
}

export interface GetScheduleSuccess {
  type: typeof GET_SCHEDULE_SUCCESS;
  payload: ScheduleState;
}

export interface GetScheduleFailure {
  type: typeof GET_SCHEDULE_FAILURE;
}

export interface SetScheduleLastUpdate {
  type: typeof SET_SCHEDULE_LAST_UPDATE;
  payload: Date;
}

export interface ScheduleState {
  isActive: boolean;
  lastUpdate: Date | null;
  nextUpdate: Date;
}

export type ScheduleActionTypes = GetScheduleRequest | GetScheduleSuccess | GetScheduleFailure | SetScheduleLastUpdate;
