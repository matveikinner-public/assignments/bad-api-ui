import { combineReducers } from 'redux';
import scheduleReducer from './schedule/schedule.reducer';
import stockReducer from './stock/stock.reducer';

export default combineReducers({
  stock: stockReducer,
  schedule: scheduleReducer,
});
