import Schedule from '@warehouse/domain/entities/schedule.model';
import StockItem from '@warehouse/domain/entities/stockItem.model';
import IWarehouseRepository from '@warehouse/domain/interfaces/warehouse.repository.interface';
import { inject, injectable } from 'inversify';
import TYPES from './di/warehouse.types';
import WarehouseRemoteRepository from './network/warehouse.remote.repository';

@injectable()
export default class WarehouseRepositoryImplementation implements IWarehouseRepository {
  @inject(TYPES.WarehouseRemoteRepository)
  private _warehouseRemoteRepository!: WarehouseRemoteRepository;

  getStockItems(productTypes: string[]): Promise<StockItem[]> {
    return this._warehouseRemoteRepository.getStockItems(productTypes);
  }

  getSchedule(): Promise<Schedule> {
    return this._warehouseRemoteRepository.getSchedule();
  }
}
