import { inject, injectable } from 'inversify';
import IWarehouseRepository from '@warehouse/domain/interfaces/warehouse.repository.interface';
import StockItem from '@warehouse/domain/entities/stockItem.model';
import Schedule from '@warehouse/domain/entities/schedule.model';
import TYPES from '../di/warehouse.types';
import WarehouseService from './warehouse.service';

@injectable()
export default class WarehouseRemoteRepository implements IWarehouseRepository {
  @inject(TYPES.WarehouseService) private _warehouseService!: WarehouseService;

  getStockItems(productTypes: string[]): Promise<StockItem[]> {
    return this._warehouseService.getStockItems(productTypes);
  }

  getSchedule(): Promise<Schedule> {
    return this._warehouseService.getSchedule();
  }
}
