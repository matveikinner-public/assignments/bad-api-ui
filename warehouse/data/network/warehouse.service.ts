import { inject, injectable } from 'inversify';
import TYPES from '@core/data/di/core.types';
import HttpService from '@core/data/network/http.service';
import StockItemResponse from '@warehouse/domain/entities/stockItemResponse.model';
import StockItem from '@warehouse/domain/entities/stockItem.model';
import ScheduleResponse from '@warehouse/domain/entities/scheduleResponse.model';
import Schedule from '@warehouse/domain/entities/schedule.model';
import IWarehouseRepository from '../../domain/interfaces/warehouse.repository.interface';

@injectable()
class WarehouseService implements IWarehouseRepository {
  @inject(TYPES.HttpService) private _httpService!: HttpService;

  async getStockItems(productTypes: string[]): Promise<StockItem[]> {
    return (
      await this._httpService
        .getClient()
        .get<StockItemResponse>('api/warehouse/stock', { params: { types: productTypes.join(',') } })
    ).data.data;
  }

  async getSchedule(): Promise<Schedule> {
    return (await this._httpService.getClient().get<ScheduleResponse>('api/warehouse/schedule')).data.data;
  }
}

export default WarehouseService;
