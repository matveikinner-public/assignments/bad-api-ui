const TYPES = {
  WarehouseService: Symbol.for('WarehouseService'),
  WarehouseRepository: Symbol.for('WarehouseRepository'),
  WarehouseRemoteRepository: Symbol.for('WarehouseRemoteRepository'),
  WarehouseInteractor: Symbol.for('WarehouseInteractor'),
  HttpService: Symbol.for('HttpService'),
};

export default TYPES;
