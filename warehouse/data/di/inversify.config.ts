import { Container } from 'inversify';
import coreContainer from '@core/data/di/inversify.config';
import IWarehouseRepository from '../../domain/interfaces/warehouse.repository.interface';
import WarehouseInteractor from '../../domain/interactors/warehouse.interactor';
import TYPES from './warehouse.types';
import WarehouseService from '../network/warehouse.service';
import WarehouseRepositoryImplementation from '../warehouse.repository.implementation';
import WarehouseRemoteRepository from '../network/warehouse.remote.repository';

// See issue about combining multiple containers
// https://github.com/inversify/InversifyJS/issues/776#issuecomment-685415856

const container = new Container();
const inheritContainer = container.createChild();
const warehouseContainer = Container.merge(coreContainer, inheritContainer);

warehouseContainer.bind<IWarehouseRepository>(TYPES.WarehouseService).to(WarehouseService);
warehouseContainer.bind<IWarehouseRepository>(TYPES.WarehouseRepository).to(WarehouseRepositoryImplementation);
warehouseContainer.bind<IWarehouseRepository>(TYPES.WarehouseRemoteRepository).to(WarehouseRemoteRepository);
warehouseContainer.bind<IWarehouseRepository>(TYPES.WarehouseInteractor).to(WarehouseInteractor);

export default warehouseContainer;
