import StockItem from '../entities/stockItem.model';
import Schedule from '../entities/schedule.model';

export default interface IWarehouseRepository {
  getStockItems(productTypes: string[]): Promise<StockItem[]>;
  getSchedule(): Promise<Schedule>;
}
