import Schedule from './schedule.model';
import StockItem from './stockItem.model';

export default class WebsocketResponse {
  statusCode!: number;
  data!: {
    schedule: Schedule;
    stock: StockItem[];
  };
}
