export default class Schedule {
  isActive!: boolean;
  lastUpdate!: number | null;
  nextUpdate!: number;
}
