export default class StockItem {
  id!: string;
  availability!: string;
  code!: string;
  type!: string;
  name!: string;
  color!: string[];
  price!: number;
  manufacturer!: string;
}
