import Schedule from './schedule.model';

export default class ScheduleResponse {
  statusCode!: number;
  data!: Schedule;
}
