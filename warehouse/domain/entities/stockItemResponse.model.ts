import StockItem from './stockItem.model';

export default class StockItemResponse {
  statusCode!: number;
  data!: StockItem[];
}
