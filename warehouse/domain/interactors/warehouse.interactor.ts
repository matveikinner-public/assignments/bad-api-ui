import { inject, injectable } from 'inversify';
import TYPES from '../../data/di/warehouse.types';
import Schedule from '../entities/schedule.model';
import StockItem from '../entities/stockItem.model';
import IWarehouseRepository from '../interfaces/warehouse.repository.interface';

@injectable()
class WarehouseInteractor {
  @inject(TYPES.WarehouseRepository) private _warehouseRepository!: IWarehouseRepository;

  async getStockItems(productTypes: string[]): Promise<StockItem[]> {
    return this._warehouseRepository.getStockItems(productTypes);
  }

  async getSchedule(): Promise<Schedule> {
    return this._warehouseRepository.getSchedule();
  }
}

export default WarehouseInteractor;
