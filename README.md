# Bad API Assignment

## Assignment

Assignment brief for junior developers

> Your client is a clothing brand that is looking for a simple web app to use in their warehouses. To do their work efficiently, the warehouse workers need a fast and simple listing page per product category, where they can check simple product and availability information from a single UI. There are three product categories they are interested in for now: gloves, facemasks, and beanies. Therefore, you should implement three web pages corresponding to those categories that list all the products in a given category. One requirement is to be easily able to switch between product categories quickly. You are free to implement any UI you want, as long as it can display multiple products at once in a straightforward and explicit listing. At this point, it is not necessary to implement filtering or pagination functionality on the page.

> The client does not have a ready-made API for this purpose. Instead, they have two different legacy APIs that combined can provide the needed information. The legacy APIs are on critical-maintenance-only mode, and thus further changes to them are not possible. The client knows the APIs are not excellent, but they are asking you to work around any issues in the APIs in your new application. The client has instructed you that both APIs have an internal cache of about 5 minutes.

## Solution

Hosted on Netlify https://badapiui.netlify.app/

Very simple UI to preview in a table (has pagination) stock items by product type (default Material UI components pretty much without additional styles). UI retrieves initial stock items independently and manages automatic updates to stock items with Websocket connection. However, user is able to retrieve data manually as well.

### Done

- Decent attempt at CLEAN architecture with SOLID principles (see https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html). The architecture is overkill, but should be able to easily scale in the future (on a conceptual level idea was to tackle issue which was present also here https://www.reaktor.com/blog/how-the-trust-of-a-product-owner-let-me-rebuild-adidas-product-pages-with-75-less-code/ and seems to be true in many modern Frontend applications)
- Websocket connection as context and custom hook to access it
- Generic table with pagination which manages several things autonomously ex. to calculate cell width
- Custom Webpack setup in TS with Brotli compression and paths between modules
- Global loading and notification messages
- Superb linters as well as TS configuration

### Learnings

- First time experimenting with Websocket client and Webpack custom configurations as I dislike some CRA features ex. validating and overwriting TSConfig (not to mention forcing JSON vs. YAML)
- There are now more questions than ever about how to properly implement and scale CLEAN architecture with React applications

### Improvements

- Disable update FAB when request is already in progress
- Decrease (or increase...) initial HTTP call timeout though original legacy API is anyways super slow to respond
- Table implementation was mostly analysis paralysis as there are so many premade alternatives. It was difficult to decide whether to go with premade solution, completely from scratch or somewhere in the middle. The decision taken to go in the middle with generic table was in retrospective poor decision as incoming data and its format was well known. In short current table solution is too generic and plain for the problem at hand
- The UI mobile scalability is horrible not to mention design - there was no "Mobile First" approach taken
- There should be separate mappers to map API responses to domain models - the data which application expects. Currently the same base models are all over the application
- Most likely architecturally overkill solution to problem at hand
- There are no tests
- Focus was too technical vs. UI / UX

## Installation

### Scripts

To run application on local machine in watch mode

    npm run start:dev

To build application on local machine in production mode

    npm run build

### Docker

To [create](https://docs.docker.com/engine/reference/commandline/image_build/) Docker image

    docker build -t <image-name>:<tag> .

To [list](https://docs.docker.com/engine/reference/commandline/images/) available Docker image(s)

    docker images

To [run](https://docs.docker.com/engine/reference/run/) Docker container

    docker run -d -p <localhost-port>:<container-port> <image-name>:<tag>

To [list](https://docs.docker.com/engine/reference/commandline/ps/) running Docker container(s)

    docker ps

To [stop](https://docs.docker.com/engine/reference/commandline/stop/) running Docker container(s)

    docker stop <container-id>

To [remove](https://docs.docker.com/engine/reference/commandline/image_rm/) Docker image(s)

    docker image rm <image-id>
