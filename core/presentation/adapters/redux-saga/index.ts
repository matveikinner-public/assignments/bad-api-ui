import { all, AllEffect, ForkEffect } from 'redux-saga/effects';
import warehouseSagas from '@warehouse/presentation/adapters/redux-saga';

export default function* rootSaga(): Generator<
  AllEffect<Generator<AllEffect<ForkEffect<never>>, void, unknown>>,
  void,
  unknown
> {
  yield all([warehouseSagas()]);
}
