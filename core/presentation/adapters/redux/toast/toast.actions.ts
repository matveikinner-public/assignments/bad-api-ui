import { CREATE_SUCCESS_TOAST, CREATE_ERROR_TOAST, RESET_TOAST } from './toast.constants';
import { ToastActionTypes } from './toast.types';

export const createSuccessToast = (payload: string): ToastActionTypes => ({
  type: CREATE_SUCCESS_TOAST,
  payload,
});

export const createErrorToast = (payload: string): ToastActionTypes => ({
  type: CREATE_ERROR_TOAST,
  payload,
});

export const resetToast = (): ToastActionTypes => ({
  type: RESET_TOAST,
});
