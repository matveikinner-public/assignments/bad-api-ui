import 'reflect-metadata';
import React from 'react';
import ReactDOM from 'react-dom';
import store from '@core/presentation/frameworks/redux.config';
import { Provider } from 'react-redux';
import App from './app/App';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
