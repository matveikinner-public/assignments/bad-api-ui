import React, {
  createContext,
  FunctionComponent,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import io from 'socket.io-client';

export const WebsocketContext = createContext<SocketIOClient.Socket | null>(null);

const WebsocketProvider: FunctionComponent<{ children: ReactNode }> = ({ children }: { children: ReactNode }) => {
  const [connection, setConnection] = useState<SocketIOClient.Socket | null>(null);

  const options: SocketIOClient.ConnectOpts = useMemo(() => ({}), []);

  useEffect(() => {
    try {
      const socketConnection = io(process.env.BASE_URL || '127.0.0.1', options);
      setConnection(socketConnection);
    } catch (error) {
      console.log(error);
    }
  }, [options]);

  return <WebsocketContext.Provider value={connection}>{children}</WebsocketContext.Provider>;
};

export const useWebsocket = (): SocketIOClient.Socket | null => {
  const ctx = useContext(WebsocketContext);
  if (ctx === undefined) {
    throw new Error('useWebsocket can only be used inside WebsocketContext');
  }
  return ctx;
};

export default WebsocketProvider;
