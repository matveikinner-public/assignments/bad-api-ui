import { LinearProgress } from '@material-ui/core';
import React, { FunctionComponent } from 'react';
import { useSelector } from 'react-redux';
import { selectLoader } from '../../adapters/redux/loader/loader.selectors';
import useStyles from './Loader.style';

const Loader: FunctionComponent = () => {
  const classes = useStyles();
  const loader = useSelector(selectLoader);

  if (loader.isActive) {
    return <LinearProgress className={classes.loader} />;
  }
  return null;
};

export default Loader;
