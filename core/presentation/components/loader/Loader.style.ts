import { createStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    loader: {
      position: 'absolute',
      top: 0,
      right: 0,
      left: 0,
    },
  })
);

export default useStyles;
