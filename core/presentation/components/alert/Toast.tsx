import React, { FunctionComponent, useState, useEffect } from 'react';
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { useSelector, useDispatch } from 'react-redux';
import { selectToastState } from '@core/presentation/adapters/redux/toast/toast.selectors';
import { resetToast } from '@core/presentation/adapters/redux/toast/toast.actions';

const Toast: FunctionComponent = () => {
  const [open, setOpen] = useState(false);
  const toast = useSelector(selectToastState);
  const dispatch = useDispatch();

  useEffect(() => {
    setOpen(true);
  }, [toast]);

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
    dispatch(resetToast());
  };

  if (toast.type) {
    return (
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert severity={toast.type} onClose={handleClose}>
          {toast.message || 'This is a success message!'}
        </Alert>
      </Snackbar>
    );
  }
  return null;
};

export default Toast;
