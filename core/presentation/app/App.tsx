import React, { FunctionComponent } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import {
  unstable_createMuiStrictModeTheme as createMuiTheme,
  CssBaseline,
  responsiveFontSizes,
  ThemeProvider,
} from '@material-ui/core';
import { v4 as uuid } from 'uuid';
import routes from './App.routes';
import themeOptions from '../theme/custom.theme';
import WebsocketProvider from '../contexts/websocket.context';

const App: FunctionComponent = () => {
  let theme = createMuiTheme({
    ...themeOptions,
    // For example to later replace theme type ('dark' | 'light) to one from global state management
    palette: { ...themeOptions.palette, type: 'light' },
  });
  theme = responsiveFontSizes(theme);

  return (
    <WebsocketProvider>
      <CssBaseline />
      <ThemeProvider theme={theme}>
        <Router>
          <Switch>
            {routes.map((route) => (
              <Route key={uuid()} {...route} />
            ))}
          </Switch>
          <Redirect to='/warehouse' />
        </Router>
      </ThemeProvider>
    </WebsocketProvider>
  );
};

export default App;
