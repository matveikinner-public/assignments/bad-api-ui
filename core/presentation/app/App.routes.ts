import { RouteProps } from 'react-router-dom';
import Warehouse from '@warehouse/presentation/pages/warehouse/Warehouse';

interface MyRouteProps extends RouteProps {
  secure?: boolean;
}

const routes: MyRouteProps[] = [
  {
    path: '/warehouse',
    exact: true,
    component: Warehouse,
  },
];

export default routes;
